# Tarea: Termina el producto de datos TSP

**Nancy Dira Martínez Guzmán**
**Programación para Ciencia de Datos**

Los cambios que realicé estan en la rama de **develop**, decidí no pasarlos a master porque no pude resolver el problema en su totalidad. 

**Comentario importante:** Algó arruiné en la configuración del ambiente de vagrant que cuando coloco calculate_tour en la terminal me sale el error:
```
Traceback (most recent call last):
  File "/home/vagrant/.pyenv/versions/tsp/bin/calculate_tour", line 33, in <module>
    sys.exit(load_entry_point('tsp', 'console_scripts', 'calculate_tour')())
  File "/home/vagrant/.pyenv/versions/tsp/bin/calculate_tour", line 22, in importlib_load_entry_point
    for entry_point in distribution(dist_name).entry_points
  File "/home/vagrant/.pyenv/versions/3.7.3/envs/tsp/lib/python3.7/site-packages/importlib_metadata/__init__.py", line 558, in distribution
    return Distribution.from_name(distribution_name)
  File "/home/vagrant/.pyenv/versions/3.7.3/envs/tsp/lib/python3.7/site-packages/importlib_metadata/__init__.py", line 215, in from_name
    raise PackageNotFoundError(name)
importlib_metadata.PackageNotFoundError: No package metadata was found for tsp
```

Pensé que era en mi repo solamente, pero cuando clono tu repo tal cuál pasa la misma cosa. Busqué en internet pero no hay mucha información de este error.

Una disculpa.